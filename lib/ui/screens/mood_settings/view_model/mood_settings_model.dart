import 'package:calendar_mood/resources/enums/mood_enum.dart';
import 'package:flutter/material.dart';

class MoodSettingsModel extends ChangeNotifier{

  double currentStressValue = 0.0;
  double currentSelfValue = 0.0;
  MoodEnum? moodItem;
  String moodDescription = '';
  TextEditingController noteController = TextEditingController();
  String saveMessage = '';


  setUpdate(){
    notifyListeners();
  }

  clearMessage(){
    saveMessage = '';
    notifyListeners();
  }

  saveData(){
    currentStressValue = 0.0;
    currentSelfValue = 0.0;
    moodItem = null;
    moodDescription = '';
    noteController.clear();
    saveMessage = 'Сохранено успешно';
    notifyListeners();
  }

}