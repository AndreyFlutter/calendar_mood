import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:calendar_mood/resources/constants/mood_constants.dart';
import 'package:calendar_mood/resources/enums/mood_enum.dart';
import 'package:calendar_mood/ui/screens/mood_settings/view_model/mood_settings_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TabMoodSettings extends StatelessWidget{
  const TabMoodSettings({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 30,
      ),
      child: Consumer<MoodSettingsModel>(
        builder: (_,viewModel,__) {
          if(viewModel.saveMessage.isNotEmpty){
            AnimatedSnackBar.rectangle(
              'Данные успешно сохранены',
              '',
              type: AnimatedSnackBarType.success,
              brightness: Brightness.light,
              animationDuration: const Duration(milliseconds: 200),
            ).show(
              context,
            );
            Future.delayed(Duration(milliseconds: 400)).then((value){
              viewModel.clearMessage();
            });
          }
          return ListView(
            shrinkWrap: true,
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                  'Что чувствуешь?',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              SizedBox(
                height: 128,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: MoodEnum.values.length,
                  itemBuilder: (context,index){
                    return Padding(
                      padding: EdgeInsets.only(
                        right: index == 5 ? 0 : 12,
                        bottom: 10,
                      ),
                      child: InkWell(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(76),
                        ),
                        onTap: (){
                          viewModel.moodItem = MoodEnum.values[index];
                          viewModel.moodDescription = '';
                          viewModel.setUpdate();
                        },
                        child: Container(
                          width: 83,
                          decoration:  BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(76),
                            ),
                            border: viewModel.moodItem == MoodEnum.values[index] ? Border.all(
                              color: const Color(0xFFFF8702),
                              width: 2,
                            ) : null,
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                spreadRadius: 0,
                                blurRadius: 10.8,
                                offset: const Offset(2, 4),
                                color: const Color(0xFFB6A1C0).withOpacity(0.11),
                              )
                            ],
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                  top: 23,
                                ),
                                child: Image.asset(
                                  MoodEnum.values[index].iconMap,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  bottom: 28,
                                ),
                                child: Text(
                                  MoodEnum.values[index].textMap,
                                  style: const TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              if(viewModel.moodItem != null)Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                ),
                child: Wrap(
                  children: List.generate(
                    MoodConstants().items.length,
                    (index) => Padding(
                      padding: const EdgeInsets.only(
                        right: 8,
                        bottom: 8,
                      ),
                      child: InkWell(
                        onTap: (){
                          viewModel.moodDescription = MoodConstants().items[index];
                          viewModel.setUpdate();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(3),
                            ),
                            color: viewModel.moodDescription == MoodConstants().items[index] ? const Color(0xFFFF8702) : Colors.white,
                            boxShadow: [
                              BoxShadow(
                                spreadRadius: 0,
                                blurRadius: 10.8,
                                offset: const Offset(2, 4),
                                color: const Color(0xFFB6A1C0).withOpacity(0.11),
                              )
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 8,
                              vertical: 3,
                            ),
                            child: Text(
                              MoodConstants().items[index],
                              style: TextStyle(
                                color: viewModel.moodDescription == MoodConstants().items[index] ? Colors.white : Colors.black,
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 20,
                  top: 36,
                ),
                child: Text(
                  'Уровень стресса',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(13),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 0,
                      blurRadius: 10.8,
                      offset: const Offset(2, 4),
                      color: const Color(0xFFB6A1C0).withOpacity(0.11),
                    )
                  ],
                ),
                child: Column(
                  children: [
                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                        activeTrackColor: Colors.orange,
                        inactiveTrackColor: Colors.grey.shade300,
                        trackHeight: 8,
                        thumbColor: Colors.orange,
                        overlayColor: Colors.orange.withAlpha(32),
                        thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 12),
                        overlayShape: const RoundSliderOverlayShape(overlayRadius: 24),
                      ),
                      child: Slider(
                        value: viewModel.currentStressValue,
                        divisions: 6,
                        max: 1,
                        min: 0,
                        onChanged: (value){
                          viewModel.currentStressValue = value;
                          viewModel.setUpdate();
                        },
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 10,
                        left: 10,
                        bottom: 16,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Низкий',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            'Высокий',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 20,
                  top: 36,
                ),
                child: Text(
                  'Самооценка',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(13),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 0,
                      blurRadius: 10.8,
                      offset: const Offset(2, 4),
                      color: const Color(0xFFB6A1C0).withOpacity(0.11),
                    )
                  ],
                ),
                child: Column(
                  children: [
                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                        activeTrackColor: Colors.orange,
                        inactiveTrackColor: Colors.grey.shade300,
                        trackHeight: 8,
                        thumbColor: Colors.orange,
                        overlayColor: Colors.orange.withAlpha(32),
                        thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 12),
                        overlayShape: const RoundSliderOverlayShape(overlayRadius: 24),
                      ),
                      child: Slider(
                        value: viewModel.currentSelfValue,
                        divisions: 6,
                        max: 1,
                        min: 0,
                        onChanged: (value){
                          viewModel.currentSelfValue = value;
                          viewModel.setUpdate();
                        },
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 10,
                        left: 10,
                        bottom: 16,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Неуверенность',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            'Уверенность',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 20,
                  top: 36,
                ),
                child: Text(
                  'Заметки',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              Container(
                height: 90,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(13),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 0,
                      blurRadius: 10.8,
                      offset: const Offset(2, 4),
                      color: const Color(0xFFB6A1C0).withOpacity(0.11),
                    )
                  ]
                ),
                child: TextFormField(
                  controller: viewModel.noteController,
                  onChanged: (val){
                    viewModel.setUpdate();
                  },
                  maxLines: 2,
                  minLines: 2,
                  decoration: const InputDecoration(
                    hintText: 'Введите заметку',
                    hintStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        style: BorderStyle.none,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 35,
                        bottom: 12
                      ),
                      child: InkWell(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(69),
                        ),
                        onTap: (){
                          viewModel.saveData();
                        },
                        child: Container(
                          height: 44,
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(69),
                            ),
                            color: viewModel.moodItem != null && viewModel.moodDescription.isNotEmpty && viewModel.noteController.text.isNotEmpty ? const Color(0xFFFF8702) : const Color(0xFFF2F2F2),
                          ),
                          child: Center(
                            child: Text(
                              'Сохранить',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                                color: viewModel.moodItem != null && viewModel.moodDescription.isNotEmpty && viewModel.noteController.text.isNotEmpty ? Colors.white : const Color(0xFFBCBCBF)
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          );
        }
      ),
    );
  }
}