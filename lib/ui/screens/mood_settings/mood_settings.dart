import 'package:calendar_mood/resources/enums/mounth_enum.dart';
import 'package:calendar_mood/ui/screens/calendar/calendar_screen.dart';
import 'package:calendar_mood/ui/screens/mood_settings/view_model/mood_settings_model.dart';
import 'package:calendar_mood/ui/screens/mood_settings/widgets/tab_mood_settings/tab_mood_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class MoodSettings extends StatelessWidget{
  const MoodSettings({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              width: 24,
            ),
            Row(
              children: [
                Text(
                  DateTime.now().day.toString(),
                ),
                Text(
                  ' ${MonthEnum.values[DateTime.now().toLocal().month - 1].nameMap}',
                ),
                Text(
                    ' ${DateTime.now().toLocal().hour.toString().length == 1 ? '0${DateTime.now().toLocal().hour.toString()}' : DateTime.now().toLocal().hour.toString()}:${DateTime.now().toLocal().minute.toString().length == 1 ?'0${DateTime.now().toLocal().minute.toString()}' : DateTime.now().toLocal().minute.toString()}'
                ),
              ],
            ),
            InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context)=>const CalendarScreen(),
                  ),
                );
              },
              child: SvgPicture.asset(
                'assets/icons/calendar_icon.svg'
              ),
            )
          ],
        ),
      ),
      body: ChangeNotifierProvider(
        create: (_)=> MoodSettingsModel(),
        child: DefaultTabController(
          length: 2,
          initialIndex: 0,
          child: Column(
            children: [
              Container(
                height: 30,
                margin: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 8,
                ),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(30),
                ),
                child: TabBar(
                  indicator: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(30),
                  ),
                  splashBorderRadius: BorderRadius.circular(30),
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.grey,
                  dividerColor: Colors.transparent,
                  tabs:  [
                    Tab(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icons/diary.svg',
                            width: 12,
                            height: 12,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          const Text(
                            'Дневник настроения',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Tab(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icons/statistics.svg',
                            width: 12,
                            height: 12,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          const Text(
                            'Статистика',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  children: [
                    const TabMoodSettings(),
                    Container(
                      color: Colors.green,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}