enum MoodEnum{
  joy,
  fear,
  rabies,
  sadness,
  calmness,
  power,
}

extension MoodEnumExtension on MoodEnum{
  static const Map<MoodEnum,String> _iconMap = {
    MoodEnum.calmness : 'assets/icons/calmness_icon.png',
    MoodEnum.fear : 'assets/icons/fear_icon.png',
    MoodEnum.joy : 'assets/icons/joy_icon.png',
    MoodEnum.power : 'assets/icons/power_icon.png',
    MoodEnum.rabies : 'assets/icons/rabies_icon.png',
    MoodEnum.sadness : 'assets/icons/sadness_icon.png',
  };

  static const Map<MoodEnum,String> _textMap = {
    MoodEnum.calmness : 'Спокойствие',
    MoodEnum.fear : 'Страх',
    MoodEnum.joy : 'Радость',
    MoodEnum.power : 'Сила',
    MoodEnum.rabies : 'Бешенство',
    MoodEnum.sadness : 'Грусть',
  };

  String get iconMap => _iconMap[this] ?? '';
  String get textMap => _textMap[this] ?? '';
}