enum MonthEnum{
  january,
  february,
  march,
  april,
  may,
  june,
  july,
  august,
  september,
  october,
  november,
  december,
}

extension MonthEnumExtension on MonthEnum{
  static const Map<MonthEnum,int> _indexMonthMap = {
    MonthEnum.january : 1,
    MonthEnum.february : 2,
    MonthEnum.march : 3,
    MonthEnum.april : 4,
    MonthEnum.may : 5,
    MonthEnum.june : 6,
    MonthEnum.july : 7,
    MonthEnum.august : 8,
    MonthEnum.september : 9,
    MonthEnum.october : 10,
    MonthEnum.november : 11,
    MonthEnum.december : 12,
  };

  static const Map<MonthEnum,String> _nameMonthMap = {
    MonthEnum.january : 'Января',
    MonthEnum.february : 'Февраля',
    MonthEnum.march : 'Марта',
    MonthEnum.april : 'Апреля',
    MonthEnum.may : 'Мая',
    MonthEnum.june : 'Июня',
    MonthEnum.july : 'Июля',
    MonthEnum.august : 'Августа',
    MonthEnum.september : 'Сентября',
    MonthEnum.october : 'Октября',
    MonthEnum.november : 'Ноября',
    MonthEnum.december : 'Декабря',
  };


  int get indexMap => _indexMonthMap[this] ?? 1;
  String get nameMap => _nameMonthMap[this] ?? '';
}